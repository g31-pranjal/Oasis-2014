/**
 * Created by 23rd and Walnut
 * www.23andwalnut.com
 * User: Saleem El-Amin
 * Date: 6/8/11
 * Time: 9:39 AM
 */

var myPlaylist = [

    {
        mp3:'mix/Beat it.mp3',
        oga:'mix/1.ogg',
        title:'Beat It',
        artist:'Michael Jackson',
        duration:'4:16',
    },
	{
        mp3:'mix/Wake Me Up When September Ends.mp3',
        oga:'mix/2.ogg',
        title:'Wake Me Up When September Ends',
        artist:'Greenday',
        duration:'4:45',
    },
	{
        mp3:'mix/I m Shipping Up to Boston.mp3',
        oga:'mix/3.ogg',
        title:'I m Shipping Up to Boston',
        artist:'Dropkick Murphys',
        duration:'2:34',
    },
	{
        mp3:'mix/One.mp3',
        oga:'mix/4.ogg',
        title:'One',
        artist:'Metallica',
        duration:'7:24',
    },
	{
        mp3:'mix/Black Hole Sun.mp3',
        oga:'mix/5.ogg',
        title:'Black Hole Sun',
        artist:'SoundGarden',
        duration:'5:20',
    }
];
